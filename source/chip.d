module chip;

import std.stdio : writeln, writefln, writef;
import std.format : format;
import std.conv;
import std.random : uniform;

static const int memStart = 0x200;
// used to clear 12 bits off the opcode
static const char[80] font =
	[
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80  // F
	];

struct Chip8
{
	int[4096] memory;
	uint[256] stack;

	char key;

	ushort op;
	
	char[16] v; // 16 2 byte registers 
	
	ushort i; // index Register
	
	ushort pc = memStart; // Program Counter
	
	ushort sp; // stack Pointer
	char soundTimer = 255;
	char delayTimer;
	bool drawFlag;
	ushort[64 * 32] gfx;

	void initialize()
	{
		foreach (i; 0 .. 0x50)
		{
			this.memory[i] = font[i];

		}
	}

	unittest
	{
		Chip8 chip = Chip8();
		chip.initialize;
		assert(chip.memory[0] == 0xF0);
		assert(chip.memory[79] == 0x80);
	}

	void read(string rom)
	{
		import std.file : exists, getSize, read;

		if (exists(rom))
		{
			writeln("Loading the Rom");
			auto romSize = getSize(rom);
			writeln("The Rom size is : ", romSize);
			if (romSize > this.memory.length - memStart)
				writefln("Rom Size is too big! romSize = %s MemSize = %s", romSize,
					this.memory.length);
			else
			{
				// is it possible to to to!int[] or do I have to use a cast here?
				this.memory[memStart..memStart+romSize] = cast(int[])read(rom);

			}
		}
		else
		{
			writeln("Cannot read ", rom);
		}

	}

	void cycle()
	{
		// Fetch
		// pad 8 zeros then fill with mem[1] values
		this.op = to!ushort(this.memory[this.pc] << 8 | this.memory[this.pc + 1]);

		switch (this.op & 0xF000) // Decode
		{
			// Execute 
		case 0x0000: // can't determine what to do for these based on the high byte
			switch (this.op & 0x000F)
			{
			case 0x0000: // 0x00E0: Clears the screen        
				// Execute opcode
				this.soundTimer--;
				break;

			case 0x000E: // 0x00EE: Returns from subroutine          
				// Execute opcode
				this.soundTimer--;
				break;

			default:
				writef("Unknown opcode [0x0000]: 0x%X\n", this.op);
				this.soundTimer--;
				break;
			}
			break;

		case 0x1000: // set the program counter to the lower 3 bytes
			this.pc = this.op & 0x0FFF;
			this.soundTimer--;
			break;

		case 0x2000: // why do we store pc in sp first?
			this.stack[this.sp] = this.pc;
			++this.sp;
			this.pc = this.op & 0x0FFF;
			this.soundTimer--;
			break;

		case 0x3000: // skip next instr if Vx = kk
			//0x3ABC
			ushort reg = this.op & 0x0F00 >> 8;
			int lowByte = this.op & 0x00FF;
			if ((this.v[reg]) == lowByte)
				this.pc += 4;
			else
				this.pc += 2;
			this.soundTimer--;
			break;

		case 0x4000: // skip instr if Vx != kk
			ushort reg = this.op & 0x0F00 >> 8;
			int lowByte = this.op & 0x00FF;
			if ((this.v[reg]) != lowByte)
				this.pc += 4;
			else
				this.pc += 2;
			this.soundTimer--;
			break;

		case 0x5000: // skip next instr if Vx = Vy
			// 5AB0 
			ushort x = this.op & 0x0F00 >> 8;
			ushort y = this.op & 0x00F0 >> 4;
			if (this.v[x] == this.v[y])
				this.pc += 4;
			else
				this.pc += 2;
			this.soundTimer--;
			break;

		case 0x6000: // Set Vx to kk
			// 6ABC
			ushort reg = this.op & 0x0F00 >> 8;
			int lowByte = this.op & 0x00FF;
			if ((this.v[reg]) == lowByte)
				this.v[reg] = to!char(lowByte);
			this.pc += 2;
			this.soundTimer--;
			break;

		case 0x7000: // Set Vx to kk
			// 7ABC
			ushort reg = this.op & 0x0F00 >> 8;
			int lowByte = this.op & 0x00FF;
			this.v[reg] = to!char(lowByte);
			this.pc += 2;
			this.soundTimer--;
			break;

		case 0x8000: //
			switch (this.op & 0x000F)
			{
			case 0x0000: // 8AB0  Set Vx to Vy
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				this.v[x] = this.v[y];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0001: // 8AB1 Set Vx = Vx | Vy
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				this.v[x] |= this.v[y];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0002: // 8AB2 Set Vx = Vx & Vy
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				this.v[x] &= this.v[y];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0003: // 8AB3 Set Vx = Vx & Vy
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				this.v[x] ^= this.v[y];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0004: // 8AB4 Set Vx = Vx set carry flag
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				this.v[x] += this.v[y];

				if (this.v[y] > (0xFF - this.v[x]))
					this.v[0xF] = 1; //carry
				else
					this.v[0xF] = 0;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0005: // 8AB5 Set Vx = Vx set carry flag
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;

				if (this.v[y] > this.v[x])
					this.v[0xF] = 1; //carry
				else
					this.v[0xF] = 0;
				this.v[x] -= this.v[y];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0006: // 8AB6 Set Vx = Vx >> 1
				ushort x = this.op & 0x0F00 >> 8;
				if (x == 1)
					this.v[0xF] = 1; //carry
				else
					this.v[0xF] = 0;
				this.v[x] = this.v[x] >> 1;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0007: // 8AB7 Set Vx = Vy - Vx
				ushort x = this.op & 0x0F00 >> 8;
				ushort y = this.op & 0x00F0 >> 4;
				if (this.v[y] > this.v[x])
					this.v[0xF] = 1; //carry
				else
					this.v[0xF] = 0;
				this.v[x] = to!char(this.v[y] - this.v[x]);
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x000E: // 8ABE Set Vx = Vx << 1
				ushort x = this.op & 0x0F00 >> 8;
				char regValue = this.v[x];

				if ((regValue & 0xF0) >> 4 == 1)
					this.v[0xF] = 1; //carry
				else
					this.v[0xF] = 0;
				this.v[x] <<= 1;
				this.pc += 2;
				this.soundTimer--;
				break;

			default:
				writef("Unknown opcode [0x0000]: 0x%X\n", this.op);
				this.soundTimer--;
				break;
			}
			break;

		case 0x9000: // 0x9ABC skip if Vx != Vy
			ushort x = this.op & 0x0F00 >> 8;
			ushort y = this.op & 0x00F0 >> 4;
			if (this.v[x] != this.v[y])
				this.pc += 4;
			else
				this.pc += 2;
			this.soundTimer--;
			break;

		case 0xA000: // ANNN: Set i to address NNN (lower 12 bits)
			this.i = this.op & 0x0FFF;
			this.pc += 2;
			this.soundTimer--;
			break;

		case 0xB000: // Set pc to NNN + v[0]
			this.pc = (this.op & 0x0FFF) + this.v[0];
			this.soundTimer--;
			break;

		case 0xC000: // set Vx to uniform(0,255) & kk
			ushort x = this.op & 0x0F00 >> 8;
			int lowByte = this.op & 0x00FF;
			char rnd = to!char(uniform(0, 255));

			this.v[x] = to!char(rnd & lowByte);
			this.pc += 2;
			this.soundTimer--;
			break;

		case 0xD000:
			// Draws to the display at 0xDXYN using vx and vy
			// Example opcode 0xD003
			ushort x = this.v[(this.op & 0x0F00) >> 8];
			ushort y = this.v[(this.op & 0x00F0) >> 4];
			ushort height = this.op & 0x000F; // Mask everything but the last byte 
			ushort pixel;

			this.v[0xF] = 0;
			foreach (ushort yline; 0 .. height)
			{
				pixel = to!ushort(this.memory[this.i + yline]);
				foreach (ushort xline; 0 .. 8)
				{
					if ((pixel & (0x80 >> xline)) != 0)
					{
						if (this.gfx[(x + xline + ((y + yline) * 64))] == 1)
							this.v[0xF] = 1;
						//ushort foo = x + xline;
						///ushort bar = y + yline;

						//ushort new_val = x + xline + ((y + yline)) * 64 ^= 1;
						//ushort new_val = (foo + bar) * 64 ^ 1; 
						this.gfx[x + xline + (y + yline) * 64] ^= 1;
					}
				}
			}
			this.drawFlag = true;
			this.pc += 2;
			this.soundTimer--;
			break;

		case 0xE000: // key
			switch (this.op & 0x00FF)
			{
			case 0x009E: // skips next instr if key stored in Vx is pressed
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				if (this.key == this.v[x])
					this.pc += 4;
				else
					this.pc += 2;
				// if (this.key[this.v[x]] != 0)
				// 	this.pc += 4;
				// else
				// 	this.pc += 2;
				this.soundTimer--;
				break;

			case 0x00A1: // same as above but skips if not pressed
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				if (this.key != this.v[x])
					this.pc += 4;
				else
					this.pc += 2;
				this.soundTimer--;
				break;
			default:
				writef("\nUnknown op code: %.4X\n", this.op);
				this.soundTimer--;
				break;
			}
			break;

		case 0xF000: // key
			switch (this.op & 0x00FF)
			{
			case 0x0007: // set register[x] to delayTimer
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.v[x] = this.delayTimer;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x000A: // set register[x] to key pressed
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.v[x] = this.key;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0015: // set delayTimer to register[x]
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.delayTimer = this.v[x];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0018: // set soundTimer to register[x]
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.soundTimer = this.v[x];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x001E: // set i += register[x]
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.i += this.v[x];
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0029: // set i to sprite location at v[x]
				ushort x = this.v[(this.op & 0x0F00) >> 8];
				this.i += this.v[x] * 0x5;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0033:
				this.memory[this.i] = this.v[(this.op & 0x0F00) >> 8] / 100;
				this.memory[this.i + 1] = this.v[(this.op & 0x0F00) >> 8] / 10;
				this.memory[this.i + 2] = this.v[(this.op & 0x0F00) >> 8] / 100;
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0055: // set memory to values in V[x]-> V[y] starting at register I
				foreach (num; 0 .. 16)
				{
					auto val = this.v[num];
					this.memory[this.i + num] = val;
				}
				this.pc += 2;
				this.soundTimer--;
				break;

			case 0x0065:
				foreach (num; 0 .. 16)
				{
					auto val = this.memory[this.i + num];
					this.v[num] = val.to!char;
				}
				this.pc += 2;
				this.soundTimer--;
				break;

			default:
				writef("\nUnknown op code: %.4X\n", this.op);
				this.soundTimer--;
				break;
			}
			break;

		default:
			writefln("Unknown opcode %X\n", this.op);
			this.soundTimer--;
			break;
		}

	}

	unittest
	{
		chip8 chip = chip8();
		chip.initialize;
		this.memory[chip.pc] = 0xA2;
		this.memory[chip.pc + 1] = 0xF0;

		chip.cycle;
		assert(chip.op == 0xA2F0);
	}

}

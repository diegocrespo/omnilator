import std.stdio;
import raylib;
import std.format : format;
import std.conv;
import chip;
import render;

static const int screenWidth = 64;
static const int screenHeight = 32;
static const int scale = 16;

void main()
{
	validateRaylibBinding();
	Chip8 chip = Chip8();
	chip.initialize;
	chip.read("source/c8_test.c8");
	Render rend = Render();
	writef("The Value of the memory is [0x0000]: 0x%X\n", chip.memory[memStart]);

	InitWindow(screenWidth * scale, screenHeight * scale, "GAME");
	SetTargetFPS(60);
	while (!WindowShouldClose())
	{
		rend.draw(chip);
		// rend.handleKey(chip);
		chip.cycle;

		if (chip.soundTimer <= 0)
			chip.soundTimer = 255;

	}
	CloseWindow();

}
